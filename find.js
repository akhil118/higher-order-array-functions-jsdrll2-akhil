
function find (elements,cb) {
    let indexValue = 0;
    if(Array.isArray(elements) && elements.length >0 ){
        for (let index = 0; index < elements.length; index++){
        
            if(cb (elements[index])){
                indexValue = index;
                break;
        }

        }

        return indexValue;

    } else {
        return "Please enter a valid input";
    }
}

module.exports = find;
