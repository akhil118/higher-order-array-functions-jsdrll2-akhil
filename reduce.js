function reduce(elements, cb) {
    if (Array.isArray(elements) && elements.length > 0){
       
            startingValue = elements[0]

            for(let index =1; index < elements.length; index++){
                startingValue=cb(startingValue,elements[index]);
            }

            return startingValue

        } 
    
    else{
        return "please enter a valid input";
    }
}

module.exports=reduce;

