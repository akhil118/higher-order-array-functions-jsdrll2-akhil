const flattenFn = require("../flatten")

const nestedArray = [1, [2], [[3]], [[[4]]]]; // use this to test 'flatten'

// const cb = element => if (Array.isArray(...element)) { cd(...element) }
// else {
//     return (...element)
// }


function cb(element) {
    if (Array.isArray(...element)) { cb(...element) }
    else {
        return (element)
}
}

const result = flattenFn(nestedArray, cb);

console.log(result);
