const each = require('../forEach');
const items = require('../arrays');

const array1 = ['a', 'b', 'c'];


const cb = (x) =>  console.log(x);

const result = each(array1,cb);     // this requires 2 inputs elements and callback function
