function filter (elements,cb){
    let filtered = [];

    if(Array.isArray(elements) && elements.length  !== 0 ){
    
    for(let element of elements){

        if(cb(element.length) === true) { 

            filtered.push(element)
            
        }
        
    }
   
        return filtered;    
    } else {
        return "Please enter a valid input";
    }


}

module.exports = filter;
